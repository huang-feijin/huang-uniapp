module.exports = {
  root: true,
  env: {
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'plugin:vue/essential', 'standard'
  ],
  plugins: [
    'vue'
  ],
  rules: {

    //在computed properties中禁用异步actions
    'vue/no-async-in-computed-properties': 'error',
    //不允许重复的keys
    'vue/no-dupe-keys': 'error',
    //不允许重复的attributes
    'vue/no-duplicate-attributes': 'warn',
    //在 <template> 标签下不允许解析错误
    'vue/no-parsing-error': ['error', {
      'x-invalid-end-tag': false
    }],
    //不允许覆盖保留关键字
    'vue/no-reserved-keys': 'error',
    //强制data必须是一个带返回值的函数
    // 'vue/no-shared-component-data': 'error',
    //不允许在computed properties中出现副作用。
    'vue/no-side-effects-in-computed-properties': 'error',
    //<template>不允许key属性
    'vue/no-template-key': 'warn',
    //在 <textarea> 中不允许mustaches
    'vue/no-textarea-mustache': 'error',
    //不允许在v-for或者范围内的属性出现未使用的变量定义
    'vue/no-unused-vars': 'warn',
    //<component>标签需要v-bind:is属性
    'vue/require-component-is': 'error',
    // render 函数必须有一个返回值
    'vue/require-render-return': 'error',
    //保证 v-bind:key 和 v-for 指令成对出现
    'vue/require-v-for-key': 'error',
    // 检查默认的prop值是否有效
    'vue/require-valid-default-prop': 'error',
    // 保证computed属性中有return语句
    'vue/return-in-computed-property': 'error',
    // 强制校验 template 根节点
    'vue/valid-template-root': 'error',
    // 强制校验 v-bind 指令
    'vue/valid-v-bind': 'error',
    // 强制校验 v-cloak 指令
    'vue/valid-v-cloak': 'error',
    // 强制校验 v-else-if 指令
    'vue/valid-v-else-if': 'error',
    // 强制校验 v-else 指令
    'vue/valid-v-else': 'error',
    // 强制校验 v-for 指令
    'vue/valid-v-for': 'error',
    // 强制校验 v-html 指令
    'vue/valid-v-html': 'error',
    // 强制校验 v-if 指令
    'vue/valid-v-if': 'error',
    // 强制校验 v-model 指令
    'vue/valid-v-model': 'error',
    // 强制校验 v-on 指令
    'vue/valid-v-on': 'error',
    // 强制校验 v-once 指令
    'vue/valid-v-once': 'error',
    // 强制校验 v-pre 指令
    'vue/valid-v-pre': 'error',
    // 强制校验 v-show 指令
    'vue/valid-v-show': 'error',
    // 强制校验 v-text 指令
    'vue/valid-v-text': 'error',
    'vue/comment-directive': 0,
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // allow async-await
    'generator-star-spacing': 'off',

    /* 重写standard */
    'no-eval': 0,
    // 去掉分号结尾检查
    semi: 0,
    /* 新加 */
    // 分号只能在末尾
    'semi-style': 2,
    // 强制symbol 必须有描述
    'symbol-description': 2,
    // 强制switch的case冒号前不能有空格
    'switch-colon-spacing': 2,
    // 强制generator 函数内有 yield
    'require-yield': 2,
    // 强制在parseInt()使用基数参数
    radix: 2,
    // 强制异步函数必须具有await表达式
    'require-await': 2,
    // 强制使用箭头函数作为回调
    'prefer-arrow-callback': 2,
    // 强制使用剩余参数代替 arguments
    'prefer-rest-params': 2,
    // 禁止没必要的拼接
    'no-useless-concat': 2,
    // 强制对象属性名在非特殊情况不使用引号
    'quote-props': [2, 'as-needed'],
    // 强制箭头函数可省略大括号时必须省略
    'arrow-body-style': [2, 'as-needed'],
    // 强制箭头参数括号可省略时必须省略
    'arrow-parens': [2, 'as-needed'],
    // 强制箭头前后必须有空格
    'arrow-spacing': 2,
    // 强制数组开/闭括号统一风格
    'array-bracket-newline': [2, 'consistent'],
    // 强制数组个数大于等于5时换行, 强制如果元素存在换行符必须换行
    'array-element-newline': [2, { multiline: true, minItems: 5 }],
    // 强制删除多余的return语句
    'no-useless-return': 2,
    // 强制不允许在变量定义之前使用它们
    'no-use-before-define': 2,
    // 禁止 return await 这个规则旨在防止由于缺乏对async function语义的理解而导致的可能的常见性能危害
    'no-return-await': 2,
    // 禁止 if 作为唯一的语句出现在 else 语句中
    'no-lonely-if': 2,

    // 建议正则表达式括号包裹
    'wrap-regex': 1,
    // 建议简化操作符
    'operator-assignment': 1,
    // 建议Promise只被Error对象拒绝
    'prefer-promise-reject-errors': 1,
    // 建议使用解构访问属性
    'prefer-destructuring': 0,
    // 建议使用扩展运算符而非 .apply()
    'prefer-spread': 1,
    // 建议使用模板字面量而非字符串连接
    'prefer-template': 1
  },
  globals: {
    uni: false
  }
}
