module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  productionSourceMap: false,
  publicPath: '/',
  assetsDir: './',
  transpileDependencies: ['@dcloudio/uni-ui'],
  chainWebpack: config => {
  },
  devServer: {
    // 端口配置
    port: 2000
  }
}
