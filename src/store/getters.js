
const getters = {
  token: state => state.system.token,
  userInfo: state => state.system.userInfo
}
export default getters
