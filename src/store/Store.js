/**
 * token验证相关
 */

const TokenKey = 'x-access-token'
const UserInfoKey = 'user-info'

/**
 * 记录
 * @param key
 * @param val
 */
export function setStore (key, val) {
  return uni.setStorageSync(key, val)
}

/**
 * 获取
 * @param key
 * @returns {any}
 */
export function getStore (key) {
  return uni.getStorageSync(key)
}

/**
 * 删除
 * @param key
 */
export function removeStore (key) {
  uni.removeStorageSync(key)
}

/**
 * 删除所有
 */
export function removeAll () {
  uni.clearStorageSync()
}

/**
 * 获取token
 * @returns {any}
 */
export function getToken () {
  return getStore(TokenKey)
}

/**
 * 记录token
 * @param token
 * @returns {any}
 */
export function setToken (token) {
  return setStore(TokenKey, token)
}

/**
 * 删除token
 */
export function removeToken () {
  removeStore(TokenKey)
}

/**
 * 获取用户信息
 * @returns {{}}
 */
export function getUserInfo () {
  const rt = getStore(UserInfoKey) || ''
  return rt ? JSON.parse(rt) : undefined
}

/**
 * 设置用户信息
 * @param userInfo
 */
export function setUserInfo (userInfo) {
  if (userInfo) {
    userInfo = JSON.stringify(userInfo)
    setStore(UserInfoKey, userInfo)
  }
}

/**
 * 删除用户信息
 */
export function removeUserInfo () {
  removeStore(UserInfoKey)
}
/**
 * 设置权限信息
 * @param permission
 */
export function setPermission (permission) {
  if (permission) {
    permission = JSON.stringify(permission)
    setStore('permission', permission)
  }
}
/**
 * 获取权限信息
 * @param permission
 */
export function getPermission (permission) {
  const rt = getStore('permission') || ''
  return rt ? JSON.parse(rt) : undefined
}
