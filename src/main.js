import Vue from 'vue'
import App from './App'
import store from './store'
import formatTime from './utils/DateFilter'
import cuCustom from './static/colorui/components/cu-custom.vue'
// 引入全局uView
import uView from '@/uview-ui'
Vue.component('cu-custom', cuCustom)
Vue.prototype.formatTime = formatTime

Vue.config.productionTip = false

Vue.use(uView)

// #ifdef MP
// 引入uView对小程序分享的mixin封装
const mpShare = require('@/uview-ui/libs/mixin/mpShare.js')
Vue.mixin(mpShare)
// #endif

App.mpType = 'app'

const app = new Vue({
  store,
  ...App
})
app.$mount()
