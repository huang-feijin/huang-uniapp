import { get, post } from '@/utils/Http';

/**
 * 登录
 * @param params
 * @returns {*}
 */
export const login = (params, config) =>
  post({
    url: '/auth/oauth/token',
    header: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    params,
    config
  });

// 退出
export const logout = params =>
  get({
    url: '/auth/oauth/logout',
    params: params || {}
  });

// 获取用户登录后基本信息
export const userInfo = params =>
  get({
    url: '/user/userInfo',
    params: params || {}
  });
