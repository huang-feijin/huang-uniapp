
const url = 'https://localhost' // 开发环境

export default {
  baseRequestUrl: `${url}`,
  photoUrl: `${url}`,
  clientId: 'saber', // 客户端id
  clientSecret: 'saber_secret' // 客户端密钥
};
